import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'opdracht4angular';
  cloud = 'Cloud';
  suite = 'Suite';
  cloudsuite = 'CloudSuite';
  processedList = new Array();
  start = 1;
  end = 100;
  loading = false;

  ngOnInit() {
    this.multipliedCheck();
  }

  mockDatabase() {
    return new Promise(resolve =>
      setTimeout(() => {
        for (let i = Number(this.start); i < Number(this.end) + 1; i++) {
          let isMultiplied5 = i % 5 == 0 && i > 5;
          let isMultiplied3 = i % 3 == 0 && i > 3;

          if (isMultiplied3 && isMultiplied5) {
            this.processedList.push('CloudSuite');
          }
          if (isMultiplied3 && !isMultiplied5) {
            this.processedList.push('Suite');
          }
          if (!isMultiplied3 && isMultiplied5) {
            this.processedList.push('Cloud');
          }
          if (!isMultiplied3 && !isMultiplied5) {
            this.processedList.push(i);
          }
        }
        resolve()
      }, 300)
    );
  }

  async multipliedCheck() {
    this.loading = true;
    this.processedList = [];
    await this.mockDatabase();
    this.loading = false;
  }
}
